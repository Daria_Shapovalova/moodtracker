package com.example.moodtracker.repository

import androidx.lifecycle.LiveData
import com.example.moodtracker.database.MoodDatabaseDao
import com.example.moodtracker.database.MoodRecording
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MoodDiaryRepository(val database: MoodDatabaseDao) {

    fun getAllRecordings(): LiveData<List<MoodRecording>> {
        return database.getAllRecordings()
    }

    // Убрать suspend
    suspend fun getYesterdayMood(): MoodRecording? {
        return withContext(Dispatchers.IO) {
            val mood = database.getLastRecording()
//            if (mood?.date != today) {
//                mood = null
//            }
            mood
        }
    }

    suspend fun insertMood(moodRecording: MoodRecording) {
        withContext(Dispatchers.IO) {
            database.insertMood(moodRecording)
        }
    }

    suspend fun updateMood(moodRecording: MoodRecording) {
        withContext(Dispatchers.IO) {
            database.updateMood(moodRecording)
        }
    }

    suspend fun clear() {
        withContext(Dispatchers.IO) {
            database.clearDatabase()
        }
    }

}