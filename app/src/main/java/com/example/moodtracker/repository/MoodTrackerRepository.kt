package com.example.moodtracker.repository

import android.app.Application
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.createDataStore
import com.example.moodtracker.database.MoodForSelection
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class MoodTrackerRepository(application: Application) {

    private var _emojis = mutableListOf(
        MoodForSelection(1, "\uD83D\uDE04", 5),
        MoodForSelection(2, "☺", 5),
        MoodForSelection(3, "\uD83E\uDD70", 5),
        MoodForSelection(4, "\uD83E\uDD73", 5),
        MoodForSelection(5, "\uD83D\uDE0E", 5),
        MoodForSelection(6, "\uD83D\uDE42", 4),
        MoodForSelection(7, "\uD83D\uDE43", 4),
        MoodForSelection(8, "\uD83E\uDD13", 4),
        MoodForSelection(9, "\uD83D\uDE0C", 4),
        MoodForSelection(10, "\uD83D\uDE05", 4),
        MoodForSelection(11, "\uD83D\uDE10", 3),
        MoodForSelection(12, "\uD83D\uDE2C", 3),
        MoodForSelection(13, "\uD83D\uDE44", 3),
        MoodForSelection(14, "\uD83D\uDE12", 3),
        MoodForSelection(15, "\uD83D\uDE34", 3),
        MoodForSelection(16, "\uD83D\uDE11", 2),
        MoodForSelection(17, "\uD83E\uDD12", 2),
        MoodForSelection(18, "\uD83E\uDD74", 2),
        MoodForSelection(19, "\uD83D\uDE15", 2),
        MoodForSelection(20, "☹️", 2),
        MoodForSelection(21, "\uD83E\uDD7A", 1),
        MoodForSelection(22, "\uD83D\uDE30", 1),
        MoodForSelection(23, "\uD83D\uDE2D", 1),
        MoodForSelection(24, "\uD83D\uDE16", 1),
        MoodForSelection(25, "\uD83D\uDE29", 1)
    )

    val emojis: List<MoodForSelection>
        get() = _emojis

    /**
     * Создает DataStore для хранения выбранного Emoji
     */
    private val dataStore: DataStore<Preferences> = application.createDataStore(
        name = "settings"
    )

    private val SELECTED_EMOJI = intPreferencesKey("selected_emoji")

    val selectedEmojiFlow: Flow<Int> = dataStore.data
        .map { preferences ->
            preferences[SELECTED_EMOJI] ?: 0
        }

    suspend fun recordSelectedEmoji(moodTypeId: Int) {
        dataStore.edit { settings ->
            settings[SELECTED_EMOJI] = moodTypeId
        }
    }

}