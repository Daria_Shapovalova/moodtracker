package com.example.moodtracker.notification

import android.app.NotificationManager
import android.util.Log
import androidx.core.content.ContextCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(p0: RemoteMessage) {
        p0.notification?.let {
            sendNotification()
        }
    }

    override fun onNewToken(p0: String) {
        Log.d("TAG", "Refreshed token: $p0")
    }

    private fun sendNotification() {
        val notificationManager = ContextCompat.getSystemService(
            application,
            NotificationManager::class.java
        ) as NotificationManager
        notificationManager.sendNotification(application)
    }

}