package com.example.moodtracker.moodtracker

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.example.moodtracker.R
import com.example.moodtracker.database.MoodForSelection

@BindingAdapter("emoji")
fun TextView.setEmoji(item: MoodForSelection?) {
    item?.let {
        text = item.emoji
    }
}

@BindingAdapter("emoji_background")
fun TextView.setBackground(selected: Boolean?) {
    let {
        if (selected == true) {
            setBackgroundResource(R.drawable.rounded_corner_selected)
        } else {
            setBackgroundResource(R.drawable.rounded_corner)
        }
    }
}


