package com.example.moodtracker.moodtracker

import android.app.NotificationChannel
import android.app.NotificationManager
import android.graphics.Color
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.example.moodtracker.R
import com.example.moodtracker.databinding.FragmentPickMoodBinding
import com.example.moodtracker.viewmodel.MoodTrackerViewModel
import com.google.firebase.messaging.FirebaseMessaging
import org.koin.android.ext.android.get

class PickMoodFragment : Fragment() {

    private var _binding: FragmentPickMoodBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_pick_mood, container, false)

        val moodTrackerViewModel = get<MoodTrackerViewModel>()

        val adapter = PickMoodAdapter(PickMoodListener { moodTypeId ->
            moodTrackerViewModel.onMoodSelection(moodTypeId)
        })
        binding.moodSelectionGrid.adapter = adapter

        adapter.data = moodTrackerViewModel.allEmojis

        moodTrackerViewModel.selectedEmojiId.observe(viewLifecycleOwner, {
            it?.let {
                adapter.selectedEmoji = it
            }
        })

//        adapter.selectedEmoji = selectedEmojiId

        binding.moodTrackerViewModel = moodTrackerViewModel

        binding.lifecycleOwner = this

        val manager = GridLayoutManager(activity, 5)
        binding.moodSelectionGrid.layoutManager = manager

        binding.commentEditText.addTextChangedListener { moodTrackerViewModel.onCommentAdded(binding.commentEditText.text.toString()) }

        createChannel(
            getString(R.string.daily_notification_channel_id),
            getString(R.string.daily_notification_channel_name)
        )

        subscribeTopic()

        return binding.root
    }

    /**
     * Notifications
     */
    private fun createChannel (channelId: String, channelName: String) {

        val notificationChannel = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH)
        notificationChannel.enableLights(true)
        notificationChannel.lightColor = Color.RED
        notificationChannel.enableVibration(true)
        notificationChannel.description = "Time to record mood"

        val notificationManager = requireActivity().getSystemService(NotificationManager::class.java)
        notificationManager.createNotificationChannel(notificationChannel)

    }

    private val TOPIC = "DailyReminder"

    private fun subscribeTopic() {
        FirebaseMessaging.getInstance().subscribeToTopic(TOPIC)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}