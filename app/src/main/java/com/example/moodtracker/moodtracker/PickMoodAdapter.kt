package com.example.moodtracker.moodtracker

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.moodtracker.database.MoodForSelection
import com.example.moodtracker.databinding.GridItemMoodSelectionBinding

class PickMoodAdapter(val clickListener: PickMoodListener) :
    RecyclerView.Adapter<PickMoodAdapter.ViewHolder>() {

    var data = listOf<MoodForSelection>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount() = data.size

    var selectedEmoji = 0
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        val selected = item.moodTypeId == selectedEmoji
        holder.bind(item, selected, clickListener)
    }

    class ViewHolder private constructor(val binding: GridItemMoodSelectionBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: MoodForSelection, selected: Boolean, clickListener: PickMoodListener) {
//            if (item.moodTypeId == selectedEmoji) {
//                item.selected = true
//            }
            binding.moodForSelection = item
            binding.clickListener = clickListener
            binding.selected = selected
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = GridItemMoodSelectionBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }

    }

}

class PickMoodListener(val clickListener: (moodTypeId: Int) -> Unit) {
    fun onClick(moodTypeId: Int) = clickListener(moodTypeId)
}