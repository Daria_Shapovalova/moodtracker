package com.example.moodtracker.viewmodel

import android.app.Application
import android.app.NotificationManager
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.*
import com.example.moodtracker.database.MoodRecording
import com.example.moodtracker.notification.sendNotification
import com.example.moodtracker.repository.MoodDiaryRepository
import com.example.moodtracker.repository.MoodTrackerRepository
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


class MoodTrackerViewModel(
    private val moodTrackerRepository: MoodTrackerRepository,
    private val moodDiaryRepository: MoodDiaryRepository,
    private val application: Application
) : ViewModel() {

    private var viewModelJob = Job()

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    var lastRecordedMood = MutableLiveData<MoodRecording?>()

    var selectedEmojiId = MutableLiveData<Int?>()

    val allEmojis = moodTrackerRepository.emojis

    init {
        initializeLastRecordedMood()
        updateSelectedEmoji()
    }

    private fun updateSelectedEmoji() {
        uiScope.launch {
            moodTrackerRepository.selectedEmojiFlow.collect { value ->
                selectedEmojiId.value = value
            }
        }
    }

    private val today =
        LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd.MM.yy")).toString()

    val commentFieldVisible = Transformations.map(lastRecordedMood) {
        when (lastRecordedMood.value?.date ?: "") {
            today -> View.VISIBLE
            else -> View.INVISIBLE
        }
    }

    private fun initializeLastRecordedMood() {
        uiScope.launch {
            lastRecordedMood.value = moodDiaryRepository.getYesterdayMood()
        }
    }

    fun onMoodSelection(moodTypeId: Int) {
        uiScope.launch {
//            if (lastRecordedMood.value?.date ?: "" != today) {
//                val newRecording = MoodRecording()
//                newRecording.mood = mood
//                insertMood(newRecording)
//            } else {
//                val yesterdayMood = lastRecordedMood.value ?: return@launch
//                yesterdayMood.mood = mood
//                updateMood(yesterdayMood)
//            }
            val newRecording = MoodRecording()
            newRecording.moodTypeId = moodTypeId
            newRecording.mood = allEmojis[moodTypeId-1].emoji
            moodDiaryRepository.insertMood(newRecording)
            moodTrackerRepository.recordSelectedEmoji(moodTypeId)
            updateSelectedEmoji()
            lastRecordedMood.value = newRecording
//            selectedEmojiId.value = moodTypeId
        }
    }



    fun onCommentAdded(comment: String) {
        uiScope.launch {
            val yesterdayMood = lastRecordedMood.value ?: return@launch
            yesterdayMood.comment = comment
            moodDiaryRepository.updateMood(yesterdayMood)
            lastRecordedMood.value = moodDiaryRepository.getYesterdayMood()
        }
    }

    fun onClear() {
        uiScope.launch {
            moodDiaryRepository.clear()
            lastRecordedMood.value = null
            _showSnackbarEvent.value = true
        }
    }

    private var _showSnackbarEvent = MutableLiveData<Boolean>()

    val showSnackbarEvent: LiveData<Boolean>
        get() = _showSnackbarEvent


    fun doneShowingSnackbar() {
        _showSnackbarEvent.value = false
    }

}




