package com.example.moodtracker.viewmodel

import androidx.lifecycle.ViewModel
import com.example.moodtracker.repository.MoodDiaryRepository

class MoodDiaryViewModel(val moodDiaryRepository: MoodDiaryRepository): ViewModel() {

    val allMoodRecordings = moodDiaryRepository.getAllRecordings()

}