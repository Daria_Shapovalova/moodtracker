package com.example.moodtracker.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.moodtracker.R
import com.example.moodtracker.databinding.FragmentSettingsBinding
import com.example.moodtracker.viewmodel.MoodTrackerViewModel
import com.google.android.material.snackbar.Snackbar
import org.koin.android.ext.android.get

class SettingsFragment : Fragment() {

    private var _binding: FragmentSettingsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false)

        val moodTrackerViewModel = get<MoodTrackerViewModel>()

        binding.moodTrackerViewModel = moodTrackerViewModel

        binding.lifecycleOwner = this

        moodTrackerViewModel.showSnackbarEvent.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                Snackbar.make(
                    requireActivity().findViewById(android.R.id.content),
                    getString(R.string.cleared_message),
                    Snackbar.LENGTH_SHORT).show()
                moodTrackerViewModel.doneShowingSnackbar()
            }
        })

        return binding.root

    }
}