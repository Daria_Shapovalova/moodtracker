package com.example.moodtracker.database

import android.graphics.Color
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

data class MoodForSelection(

    var moodTypeId: Int = 0,

    var emoji: String = "",

    var rating: Int = 0,

    var frequency: Int = 0,

    var selected: Boolean = false

)