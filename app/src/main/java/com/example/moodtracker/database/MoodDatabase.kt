package com.example.moodtracker.database

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    entities = [MoodRecording::class],
    version = 7,
    exportSchema = false
)
abstract class MoodDatabase : RoomDatabase() {

    abstract val moodDatabaseDao: MoodDatabaseDao

}