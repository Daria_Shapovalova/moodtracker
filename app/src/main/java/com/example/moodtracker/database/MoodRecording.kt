package com.example.moodtracker.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@Entity(tableName = "mood_diary_table")
data class MoodRecording (

    @PrimaryKey(autoGenerate = true)
    var recordingId: Long = 0L,

    @ColumnInfo
    val date: String = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd.MM.yy")).toString(),

    @ColumnInfo
    var mood: String = "",

    @ColumnInfo
    var moodTypeId: Int = 0,

    @ColumnInfo
    var comment: String = ""

)
