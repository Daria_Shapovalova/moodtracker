package com.example.moodtracker.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface MoodDatabaseDao {

    @Insert
    fun insertMood(moodRecording: MoodRecording)

    @Update
    fun updateMood(moodRecording: MoodRecording)

    @Query("SELECT * FROM mood_diary_table WHERE recordingId = :key")
    fun getMood(key: Long): MoodRecording

    @Query("DELETE FROM mood_diary_table")
    fun clearDatabase()

    @Query("SELECT * FROM mood_diary_table ORDER BY recordingId DESC")
    fun getAllRecordings(): LiveData<List<MoodRecording>>

    @Query("SELECT * FROM mood_diary_table ORDER BY recordingId DESC LIMIT 1")
    fun getLastRecording(): MoodRecording?

}