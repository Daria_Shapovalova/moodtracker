package com.example.moodtracker.app

import android.app.Application
import android.content.Context
import com.example.moodtracker.koin.appModule
import com.example.moodtracker.koin.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MyApp: Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        startKoin {
            androidContext(this@MyApp)
            modules(listOf(appModule, viewModelModule))
        }
    }

    companion object {
        var instance: MyApp? = null
            private set
        val context: Context?
            get() = instance
    }

}