package com.example.moodtracker.mooddiary

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.example.moodtracker.database.MoodRecording

@BindingAdapter("moodRecorded")
fun TextView.setMood(item: MoodRecording?) {
    item?.let {
        text = item.mood
    }
}

@BindingAdapter("commentRecorded")
fun TextView.setComment(item: MoodRecording?) {
    item?.let {
        text = item.comment
    }
}

@BindingAdapter("dateRecorded")
fun TextView.setDate(item: MoodRecording?) {
    item?.let {
        text = item.date
    }
}