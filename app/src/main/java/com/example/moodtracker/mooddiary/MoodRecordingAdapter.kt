package com.example.moodtracker.mooddiary

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.moodtracker.database.MoodRecording
import com.example.moodtracker.databinding.ListItemMoodRecordingBinding
import java.time.Month
import java.time.Year

class MoodRecordingAdapter : RecyclerView.Adapter<MoodRecordingAdapter.ViewHolder>() {

    var data = listOf<MoodRecording>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount() = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        holder.bind(item)
    }

    class ViewHolder private constructor(val binding: ListItemMoodRecordingBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: MoodRecording) {
            binding.moodRecording = item
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemMoodRecordingBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }

    }

}

/**
 * Заготовка для заголовков-месяцев в дневнике настроения
 */
sealed class DataItem {

    data class MoodRecordingItem(val moodRecording: MoodRecording): DataItem() {
        override val id = moodRecording.recordingId
    }
    data class Header(val month: Month, val year: Year): DataItem() {
        override val id = Long.MIN_VALUE
    }

    abstract val id: Long

}

