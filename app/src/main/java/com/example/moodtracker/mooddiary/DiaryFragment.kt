package com.example.moodtracker.mooddiary

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.moodtracker.R
import com.example.moodtracker.databinding.FragmentDiaryBinding
import com.example.moodtracker.viewmodel.MoodDiaryViewModel
import org.koin.android.ext.android.get

class DiaryFragment : Fragment() {

    private var _binding: FragmentDiaryBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_diary, container, false)

        val moodDiaryViewModel = get<MoodDiaryViewModel>()

        val adapter = MoodRecordingAdapter()
        binding.moodRecordingsList.adapter = adapter

        moodDiaryViewModel.allMoodRecordings.observe(viewLifecycleOwner, Observer {
            it?.let {
                adapter.data = it
            }
        })

        binding.moodDiaryViewModel = moodDiaryViewModel

        binding.lifecycleOwner = this

        return binding.root
    }

}