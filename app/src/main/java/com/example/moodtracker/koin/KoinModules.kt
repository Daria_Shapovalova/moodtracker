package com.example.moodtracker.koin

import androidx.room.Room
import com.example.moodtracker.app.MyApp
import com.example.moodtracker.database.MoodDatabase
import com.example.moodtracker.repository.MoodDiaryRepository
import com.example.moodtracker.repository.MoodTrackerRepository
import com.example.moodtracker.viewmodel.MoodDiaryViewModel
import com.example.moodtracker.viewmodel.MoodTrackerViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    single {
        Room.databaseBuilder(
            androidApplication(),
            MoodDatabase::class.java,
            "mood_history_database"
        )
            .fallbackToDestructiveMigration().build()
    }

    single { get<MoodDatabase>().moodDatabaseDao }

    single { MoodTrackerRepository(get()) }

    single { MoodDiaryRepository(get()) }

    single { MyApp() }

}

val viewModelModule = module {

    viewModel { MoodTrackerViewModel(get(), get(), get()) }

    viewModel { MoodDiaryViewModel(get()) }

}